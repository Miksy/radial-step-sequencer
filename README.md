# RADIAL STEP SEQUENCER
# SÉQUENCEUR CIRCULAIRE

Remarques Jérémie:
- Les nombres dans chaque cercle sont-ils utiles ?
- Le haut du cercle (à midi) arrive au centre d'une tranche et non sur une arrête. À tester dans l'autre sens. (il me semble que le cercle est plus)
- problème de temps, 1 mesure sur 3 est décalée, à tester avec la nouvelle version.
- pastilles plus petites ?

- créer des partitions circulaires --> poster (postères, des affiches quoi)
- donner des instrus dans les mains des gens pour gérer + de sons.


Remarque de Bapt:
Que le public puisse changer lui-même les kits (dub / techno etc...) avec des gros boutons parce qu'on aime bien les gros boutons.

Remarque d'Omar:
- 1 dalle peut avoir plusieurs commandes (ex: haut un son, bas un autre son, gauche change direction du sequenceur, droite skip)
- plusieurs séquenceurs intéressants: [buchla](https://tiptopaudio.com/buchla/) 245t, Makenoise René, Makenoise 0ctrl

## Todo pour la v2

### Humans
- ~Virer le "+" et replacer le start si nécessaire~
- Explication pour double click / double tap = Créer une pastille et double tap sur pastille = supprimer une pastille
- ~tester double tap sur mobile~

### Samples
- ajouter les kits de samples de nadir
- possible de choisir le sample de chaque instru ? (1 sélecteur pour chaque instru ET un global pour changer tout le kit ?)

### Midi
~Debugger la synchro Midi, assez souvent à la masse et pas du tout carrée.~

### Sauvegarde
- ~import + export de json~
- ~save on quit possible ?~ DONE
- gérer les noms de tracks trop longues
- revoir les tèrmes (wording)

## Interface et contrôles
- Hierarchiser et redesigner les contrôles (desktop et mobile)
- ~Linéariser le Lowpass~
- séparer la sauvegarde du reste ?
- ~bouton sync~

### ajouter page crédits 
À créditer: Nadir, Jérémie

### ajouter how-to
- ajouter avec les crédits
- partie "tuto" à la première entrée ?
- bouton tuto ?
- explication pour la sauvegarde

### video tuto

### ko-fi ?

### SEO
- opengraph, twitter cards, https://docs.joinmastodon.org/entities/PreviewCard/#link
- share image