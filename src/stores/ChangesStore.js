import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useChangesStore = defineStore('changes', () => {
  
  const trackChanged = ref(false)
  const shouldPrompt = ref(false)

  function record() {
    trackChanged.value = true
  }

  function cancel() {
    trackChanged.value = false
  }

  function saved() {
    trackChanged.value = false
    shouldPrompt.value = true
  }

  function exported() {
    shouldPrompt.value = false
  }

  function reset() {
    trackChanged.value = false
    shouldPrompt.value = false
  }

  return { record, cancel, saved, exported, reset, trackChanged, shouldPrompt }

})