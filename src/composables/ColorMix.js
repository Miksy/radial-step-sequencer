const parseColor = col => {
    const m = col.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i)
    return [m[1], m[2], m[3]]
}

export const colorMix = (_colorStart, _colorEnd, mix = 0) => {
    if(!_colorStart.startsWith('rgb(') || !_colorEnd.startsWith('rgb('))
        return

    const returnColor = []

    const colorStart = parseColor(_colorStart)
    const colorEnd = parseColor(_colorEnd)

    returnColor[0] = colorStart[0] * (1 - mix) + colorEnd[0] * mix
    returnColor[1] = colorStart[1] * (1 - mix) + colorEnd[1] * mix
    returnColor[2] = colorStart[2] * (1 - mix) + colorEnd[2] * mix

    return `rgb(${returnColor[0]}, ${returnColor[1]}, ${returnColor[2]})`
}