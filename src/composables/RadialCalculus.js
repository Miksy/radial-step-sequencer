
const getDistanceSquared = (coords, center) => {

  const _x = coords.x - center.x
  const _y = center.y - coords.y

  const distanceSquared = _x * _x + _y * _y

  return distanceSquared
}


const isInsideBounds = (distanceSquared, radius, innerRadius) => {

  if (distanceSquared > radius * radius) return false
  if (distanceSquared < innerRadius * innerRadius) return false

  return true

}

const magnitudeAlongTranche = (distSq, radius, innerRadius) => {

  const trancheSize = radius - innerRadius

  return (Math.sqrt(distSq) - innerRadius) / trancheSize

}

export const getAngle = (coords, center, steps) => {

  const _x = coords.x - center.x
  const _y = center.y - coords.y

  // Get angle between y axis and line [origin - point]
  // from -PI to PI
  let angle = Math.atan2(_x, _y)

  // Remap from 0 to 1
  angle = (angle / 2 + Math.PI) % Math.PI / Math.PI

  // rotate half a tranche counter clockwise (to align 1st tranche center to y axis)
  angle = (angle + 0.5 / steps) % 1

  // change direction
  // angle = 1 - angle

  return angle
}


export const getTranche = (coords, center, radius, innerRadius, steps) => {

  const distSq = getDistanceSquared(coords, center)
  if (!isInsideBounds(distSq, radius, innerRadius)) return { tranche_id: null, magnitude: null }

  const _angle = getAngle(coords, center, steps)
  const magnitude = magnitudeAlongTranche(distSq, radius, innerRadius)

  return { tranche_id: Math.floor(_angle * steps), magnitude: magnitude }

}
