import './assets/main.sass'

import { createApp } from 'vue'
import App from './App.vue'
import VueClick from 'vue-click'
import { createPinia } from 'pinia'

const pinia = createPinia()

createApp(App)
	.use(VueClick)
	.use(pinia)
	.mount('#app')